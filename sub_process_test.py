import sys
import boto3
import time


start = time.time()
last_start = time.time()
s3 = boto3.resource("s3")
s3_client = boto3.client("s3")
#bucket_name = "naviga-xml"
bucket_name = "accrete-naviga-source-docs"                
paginator = s3_client.get_paginator("list_objects_v2")
bucket = s3.Bucket(bucket_name)
list_of_prefixes = []
start_prefix = "202302"
total_processed = 0
total_english = 0
total_foreign = 0
curr_counter = 0
language_dict = {}
if int(sys.argv[1]) <= 9:
    start_prefix = start_prefix + "0" + sys.argv[1]
else:
    start_prefix = start_prefix + sys.argv[1]
    
output_file_name = start_prefix + "_language.txt"
response = paginator.paginate(Bucket=bucket_name, Prefix=start_prefix, PaginationConfig={"PageSize": 1000})

for page in response: 
    print("getting 1000 files from s3")
    files = page.get("Contents")
    
    length_of_page = len(files) # if full page is retreived, should get 1000 
    for file in files:
        file_name = file["Key"]
        data = s3_client.get_object(Bucket=bucket_name, Key=file_name)
        xml_body = data['Body'].read() 
        lines_decode = xml_body.decode("utf-8")

        total_processed += 1
        curr_counter += 1
        
        first_language_tag_ind = lines_decode.find("<xn:language>")
        second_language_tag_ind = lines_decode.find("</xn:language>", first_language_tag_ind+13)
        
        if first_language_tag_ind == -1:
            language_dict["Unknown"] = 1 + language_dict.get("Unknown", 0)
        
        else:
            language = lines_decode[first_language_tag_ind+13:second_language_tag_ind]
            language_dict[language] = 1 + language_dict.get(language, 0)
            if language != "en":
                total_foreign += 1
            
        end = time.time()
        time_diff = end - start 
        convert = time.strftime("%H:%M:%S", time.gmtime(time_diff))
        
            
        print("Current filename:", file_name)
        print("Total Processed:", total_processed)
        total_english_print = language_dict.get("en", 0)
        print("Current Counter:", curr_counter)
        print("Total English:", total_english_print)
        print("Total Foreign:", total_foreign)
        print("Total Languages:", language_dict)
        print("Time Elapsed:", convert)
    
        if curr_counter == 1000 or (curr_counter == length_of_page and length_of_page < 1000): # test if current counter, or if there are less than 1000 files on the page
            language_list = [(k, v) for k, v in language_dict.items()]
            language_list.sort(key = lambda i:i[1], reverse = True)
            output_file = open(output_file_name, "a")
            output_file.write(f"Total Processed: {total_processed}\n")
            percent_en = round((language_dict.get("en", 0)/total_processed)*100, 3)
            percent_non_en = round((total_foreign/total_processed)*100, 3)
            output_file.write(f"Total English: {total_english_print}, {percent_en}%\n")
            output_file.write(f"Total Non-English: {total_foreign}, {percent_non_en}%\n")
            output_file.write(f"Total by Non-English: \n")
            skip_first = True
            for (k, v) in language_list:
                if not skip_first:
                    percent_non = round((int(v)/total_processed)*100, 3)
                    output_file.write(f"{k}: {v}, {percent_non}%\n")
                else:
                    skip_first = False
                                
            
            output_file.write(f"Time Elapsed Since Start: {convert} \n")
            output_file.write(f"=================================================\n")
            output_file.close()
            curr_counter = 0
                
        #if contains_chinese:
        #    break
        print("# * 10")


print("done")